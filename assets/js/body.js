const about_jmb = document.getElementById("about_jmb");
const work_jmb = document.getElementById("work_exp_jmb");
const skills_jmb = document.getElementById("skills_jmb");
const contact_h1 = document.getElementById("contact_info");
const contact_h2 = document.getElementById("contact_info_id");



const about = document.querySelector('.about');
const work = document.querySelector('.work');
const skills = document.querySelector('.skills');
const contact = document.querySelector('.con_me_about');

about.onclick = function() {
	var cur_color = about_jmb.style.backgroundColor;
	
	about_jmb.style.backgroundColor = "wheat";
	setTimeout(function(){ about_jmb.style.backgroundColor = cur_color; }, 700);
}

work.onclick = function() {
	var cur_color = work_jmb.style.backgroundColor;
	
	work_jmb.style.backgroundColor = "wheat";
	setTimeout(function(){ work_jmb.style.backgroundColor = cur_color; }, 700);
}

skills.onclick = function() {
	var cur_color = skills_jmb.style.backgroundColor;

	
	skills_jmb.style.backgroundColor = "wheat";
	setTimeout(function(){ skills_jmb.style.backgroundColor = cur_color; }, 700);
}

contact.onclick = function() {
	var cur_color1 = contact_h1.style.backgroundColor;
	var cur_color2 = contact_h2.style.backgroundColor;

	
	contact_h1.style.backgroundColor = "#FFFAFA";
	contact_h2.style.backgroundColor = "#FFFAFA";
	setTimeout(function(){ contact_h1.style.backgroundColor = cur_color1; }, 700);
	setTimeout(function(){ contact_h2.style.backgroundColor = cur_color2; }, 700);
}



	